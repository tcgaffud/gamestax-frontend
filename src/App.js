import Registration from "./components/pages/Registration"

function App() {
  return (
    <div className="App">
      <Registration/>
    </div>
  );
}

export default App;
