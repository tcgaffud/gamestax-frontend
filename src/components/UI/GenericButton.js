import React from "react"
import {Button} from "react-bootstrap"

const GenericButton = (props) => {
	return (
		<Button 
			variant={props.variant}
			type = {props.type || "button"}
			onClick = {props.onClick}
		>
			{props.children}
		</Button>
	)
}

export default GenericButton