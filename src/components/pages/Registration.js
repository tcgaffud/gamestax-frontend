import React, { useRef } from 'react'
import {Container, Form} from "react-bootstrap" 
import classes from "./Registration.module.css"
import GenericButton from "../UI/GenericButton"

const Registration = (props) => {

	const userNameRef = useRef()
	const emailRef = useRef()
	const passwordRef = useRef()
	const retypeRef = useRef()



	return (
		<Container>
			<h1>Gamestax</h1>
			<h2>Signup</h2>
			<Form>
				<Form.Group>
					<Form.Control id="username" type="text" label="Username" placeholder="Username" ref={userNameRef} className={`${classes.formControl} mb-2`}/>
				</Form.Group>
				<Form.Group>
					<Form.Control id="email" type="email" label="Email" placeholder="Email" ref={emailRef} className={`${classes.formControl} mb-2`}/>
				</Form.Group>
				<Form.Group>
					<Form.Control id="password" type="password" label="Password" placeholder="Password" ref={passwordRef} className={`${classes.formControl} mb-2`}/>
				</Form.Group>
				<Form.Group>
					<Form.Control id="retype" type="password" label="Retype" placeholder="Retype" ref={retypeRef} className={`${classes.formControl} mb-2`}/>
				</Form.Group>
				<div className="d-grid gap-2 mb-3">
					<GenericButton type="submit" variant="info" onClick="">
						Sign me up!
					</GenericButton>
				</div>
				<div className="d-grid gap-2 mb-3">
					<GenericButton type="submit" variant="danger" onClick="">
						Sign in with G+
					</GenericButton>
				</div>
				<div className="d-grid gap-2">
					<GenericButton type="submit" variant="primary" onClick="">
						Sign in with Facebook
					</GenericButton>
				</div>
			</Form>
		</Container>
	)
}

export default Registration